Mailbuild
=========

Mailbuild integration. http://www.mailbuild.com


Mailbuild is a popular mailing list delivery and subscription management
service. This module defines a block for each configured Mailbuild List.

TO USE THIS MODULE YOU MUST HAVE AN ACCOUNT WITH MAILBUILD.COM

This block allows your users to subscribe to your list by enter their email
address (which defaults to $user->mail).
       
SOAP client functionality is provides via the NuSOAP library. For more
information on NuSOAP, head to the SF.net page: 
http://sourceforge.net/projects/nusoap/

This module was developed by Spoon Media http://www.spoon.com.au

Installation
------------

- This module requires the NuSOAP library, which is available from
http://sourceforge.net/projects/nusoap/

- Install the NuSOAP library to the module's directory so that the
directory structure looks like this:

  mailbuild/nusoap
  mailbuild/nusoap/nusoap.txt
  mailbuild/nusoap/lib
  mailbuild/nusoap/lib/nusoap.php

Configuration
-------------

To use this module, you will need your Mailbuild 'Service URL', API Key
and one or more List ID's.

Enter these into the module's settings page.

For instructions on how to retrieve this information, see the Mailbuild
API documentation: http://mailbuild.com/api/

